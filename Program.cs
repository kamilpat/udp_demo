﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace UPD_Anchor1
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                UdpClient udpClient = new UdpClient("127.0.0.1", 9999);
                Byte[] sendBytes = Encoding.ASCII.GetBytes("ANCHOR%NR=1&X=0,25&Y=0,25&V=3.41\n");
                Byte[] sendBytesSecond = Encoding.ASCII.GetBytes("ANCHOR%NR=3&X=2,25&Y=3,25&V=3.41\n");
                Byte[] sendBytesThird = Encoding.ASCII.GetBytes("ANCHOR%NR=3&X=2,25&Y=4,25&V=3.41\n");
                Byte[] tag = Encoding.ASCII.GetBytes("TAG%NR=1&X=3&Y=4&V=1,8\n");

                udpClient.Send(sendBytes, sendBytes.Length);
                udpClient.Send(sendBytesSecond, sendBytesSecond.Length);
                udpClient.Send(sendBytesThird, sendBytesThird.Length);
                udpClient.Send(tag, tag.Length);

                Console.WriteLine( "Wysłane");
               Thread.Sleep(1000);
            } while (true);



        }
    }
}
